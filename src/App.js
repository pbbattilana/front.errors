import './App.css';
import { Outlet } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Welcome to <span className="name">RAE</span></h1>
        <p>
          Share errors, solutions and be happy.
        </p>
        <div className="App-link">
          <a
            className="button"
            href= '/errors'
          >
            Errors
          </a>
          <a
            className="button"
            href="/solutions"
          >
            Solutions
          </a>
        </div>
        <Outlet />
      </header>
    </div>
  );
}

export default App;
