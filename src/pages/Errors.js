import "./Errors.css";
import "../components/Table.css";
import Table from "../components/TableErrors";
import { useEffect, useState } from 'react';


function Errors() {
  const url = 'http://localhost:8080/errors/getErrors'
  const [todos, setTodos] = useState()
  const fetchApi = async () => {
    const response = await fetch(url)
    //console.log(response.json())
    const responseJSON = await response.json()
    setTodos(responseJSON)
  }
  useEffect(() => {
    fetchApi()
  }, [])
  return (
    <div className="App">
      <h1 className="text">Errors</h1>
      <section class="wrapper">
        <main class="row title">
          <ul>
            <li>Nivel</li>
            <li>Codigo</li>
            <li>Encabezado</li>
            <li>Editar</li>
            <li>Eliminar</li>
          </ul>
        </main>
        <section class="row-fadeIn-wrapper">
        <article class="row fadeIn nfl">
          {
            !todos ? <Table></Table> :
              todos.data.map((todo, index) => {
                return <ul key="{todo.id}">
                    <li>{todo.tipo_error}</li>
                    <li>{todo.codigo}</li>
                    <li>{todo.encabezado}</li>
                    <li><a href="" class="material-symbols-outlined">Autorenew</a></li>
                    <li><a href="" class="material-symbols-outlined">Delete</a></li>
                    <li class="more-content">{todo.log}</li>
                  </ul>
              })
          }
          </article>
        </section>
      </section>
    </div >
  );
}

export default Errors;
