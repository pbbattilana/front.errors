import './Errors.css';
import "../components/Table.css";
import Table from "../components/TableSolutions";
import { useEffect, useState } from 'react';

function Solutions() {
  const url = 'http://localhost:8080/solutions'
  const [todos, setTodos] = useState()
  const fetchApi = async () => {
    const response = await fetch(url)
    //console.log(response.json())
    const responseJSON = await response.json()
    setTodos(responseJSON)
  }
  useEffect(() => {
    fetchApi()
  }, [])
  return (
    <div className="App">
      <h1 className="text">Solutions</h1>
      <section class="wrapper">
        <main class="row title">
          <ul>
            <li>ID Error</li>
            <li>Descripcion Error</li>
          </ul>
        </main>
        <section class="row-fadeIn-wrapper">
          <article class="row fadeIn nfl">
          <ul>
            {
              !todos ? <Table></Table> :
                todos.data.map((todo, index) => {
                  return <ul key="{todo.id}"><li>{todo.id}</li><li>{todo.descripcion}</li></ul>
                })
            }
          </ul>
        </article>
      </section>
    </section>
    </div >
  );
}

export default Solutions;
