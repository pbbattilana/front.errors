import "./Table.css";

function TableSolutions() {
  return (
    <div>
      <section class="wrapper">
        <main class="row title">
          <ul>
            <li>Nivel</li>
            <li>Codigo</li>
            <li>Errores</li>
            <li>Agregar</li>
            <li>Editar</li>
          </ul>
        </main>
        <section class="row-fadeIn-wrapper">
          <article class="row fadeIn nfl">
            <ul>
              <li>
                <a href="#">Fatal</a>
              </li>
              <li>1111</li>
              <li>12</li>
              <li></li>
              <li></li>
            </ul>
            <ul class="more-content">
              <li>
                This 1665-player contest boasts a $300,000.00 prize pool and
                pays out the top 300 finishing positions. First place wins
                $100,000.00. Good luck!
              </li>
            </ul>
          </article>
        </section>
        
        <article class="row nfl">
          <ul>
            <li>
              <a href="#">Warning</a>
            </li>
            <li>1234</li>
            <li>48</li>
            <li></li>
            <li></li>
          </ul>
          <ul class="more-content">
            <li>
              This 1665-player contest boasts a $300,000.00 prize pool and pays
              out the top 300 finishing positions. First place wins $100,000.00.
              Good luck!
            </li>
          </ul>
        </article>
        <article class="row mlb update-row">
          <ul>
            <li>
              <a href="#">Fatal</a>
            </li>
            <li>1010</li>
            <li>28</li>
            <li></li>
            <li></li>
          </ul>
          <ul class="more-content">
            <li>
              This 1665-player contest boasts a $300,000.00 prize pool and pays
              out the top 300 finishing positions. First place wins $100,000.00.
              Good luck!
            </li>
          </ul>
        </article>
        <article class="row mlb update-row">
          <ul>
            <li>
              <a href="#">Warning</a>
            </li>
            <li>3241</li>
            <li>30</li>
            <li></li>
            <li></li>
          </ul>
          <ul class="more-content">
            <li>
              This 1665-player contest boasts a $300,000.00 prize pool and pays
              out the top 300 finishing positions. First place wins $100,000.00.
              Good luck!
            </li>
          </ul>
        </article>
      </section>
    </div>
  );
}

export default TableSolutions;
